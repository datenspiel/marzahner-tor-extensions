module WordifyCms
  class HotSpotTag < Tag
    require 'html/sanitizer'

    register_tag 'hot_spots'

    def initialize(tag_name, markup, tokens)
      @hot_spot_navigation = args(markup)
      super
    end

    def render(context)
      collect_hot_spots(context)
    end

    private

    def collect_hot_spots(context)
      hot_spot_klass_name = @hot_spot_navigation.camelize.singularize
      hot_spot_klass      = WordifyCms::DbModel.load(:name => hot_spot_klass_name)
      hot_spots           = hot_spot_klass.send(@hot_spot_navigation)

      begin
        hot_spot_navigation = WordifyCms::Navigation.where(:identifier => @hot_spot_navigation).
                                                    first
        spots = hot_spots.inject([]) do |result, element|
          liquidable = element.to_liquid
          title      = element.get_attribute("title")
          name       = extract_name(title, :remove_new_lines => true)
          name       = HTMLEntities.new.decode(name)
          nav_elem = hot_spot_navigation.nodes.roots.detect do |node|
            extracted_name = extract_name(node.name, :remove_new_lines => true)
            en = extracted_name.gsub(/\s/, "").downcase
            qn = name.gsub(/\s/, "").downcase    
            en == qn
          end
          
          if nav_elem.present?
            liquidable["href"]    = "#{nav_elem.url}?hotspot"
            liquidable["nav_id"]  = nav_elem.id
            result << liquidable
          end
          result
        end
        
      rescue => e
        Rails::logger.info e.message
        spots = []
      ensure
        file_name           = "/wordify_cms/templates/spots"

        return render_slim( context, 
                            file_name,
                            :hot_spots => spots
                          )
      end
    end

    def extract_name(*args)
      options = args.extract_options!
      str     = args.shift
      sanitizer = HTML::FullSanitizer.new
      sanitized = sanitizer.sanitize(str)
      
      if options[:remove_new_lines]
        sanitized.gsub!(/\n/,"")
      end
      return sanitized 
    end

  end
end