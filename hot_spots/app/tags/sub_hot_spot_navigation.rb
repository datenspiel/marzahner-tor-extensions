module WordifyCms
  class SubHotSpotNavigation < Tag
    include WordifyCms::SpecialChars
    include WordifyCms::PageFindable
    register_tag 'sub_hot_spot_navigation'

    def initialize(tag_name, markup, tokens)
      @collection_name = args(markup)
      super
    end

    def render(context)
      slug            = context.registers[:params][:slug]
      hot_spot_active = context.registers[:params].has_key?("hotspot")
      if hot_spot_active
        p "ho"
        begin
          result  = traverse_to_find_page_by_alias(slug)
          page    = result.delete(:page)
          navigation = navigation_for_alias(slug)
          context.registers[:params].merge!(result)
        rescue => e
          begin
            result  = traverse_to_find_page_by_path(slug)
            page    = result.delete(:page)
          
          #context.registers[:page] = page
            context.registers[:params].merge!(result)
            navigation = result[:navigation]
            p "the pa"
          rescue => fe
            p fe.message
          end
        ensure
          #html      = navigation_template
          #compiled  = Liquid::Template.parse(html).render(context)
          file_name = '/wordify_cms/templates/spots_navigation_page'
          list_css = {
            :ul_class => "nav nav--stacked",
            :item_class => "nav__item",
            :item_active_class => "nav__item--active"
          }
          compiled = render_slim(context, 
                    file_name,
                      :root       => navigation,
                      :page_id    => page.id,
                      :css        => list_css,
                      :registers  => context.registers,
                      :hot_spot_id => navigation.id
                    )
          return compiled
        end
      else
        if @collection_name.present?
          html      = template(@collection_name, @args["page_slug"])
        else
          html      = navigation_template
        end
        compiled  = Liquid::Template.parse(html).render(context)
        return compiled
      end
    end

    private 

    def navigation_template
      <<-DOC
        {% navigation_for_page ' "class": "nav nav--stacked" | "item_class" : "nav__item" | "item_active_class" : "nav__item--active" | "back": "true"'%}
      DOC
    end

    def template(collection,slug)
      <<-DOC
      {% list_for '#{collection} | "page_slug" : "#{slug}" | "order_by" : "id desc"' %}
            <ul class="nav nav--stacked">
                <li class="nav__item nav__item--active">
                    <a href="/neuigkeiten">Neuigkeiten</a>
                        <ul class="nav nav--stacked">
                            {% for news in posts %}
                                {% capture news_date %}
                                    {{ news.date_created | date }}
                                {% endcapture %}
                                <li class="nav__item">
                                    <a href="{{news.page_alias_url}}">
                                        {{news_date}}
                                    </a>
                                </li>
                            {%endfor%}
                        </ul>
                </li>
            </ul>
        {%endlist_for%}
      DOC
    end
  end
end