class ListOfContacts < WordifyCms::Extension

  register_extension

  config do |c|
    c.add_routes do
      match "/departments/for/scope/:id" => "departments#get_for_scope", :via => :get
    end

  end

end

#WordifyCms::ExtensionEngine.register_extension ListOfContacts