# List of contacts extension

Adds a list of contacts to Marzahner Tor.

![Contact List](http://f.cl.ly/items/3D3z083p0F19010J1p0G/Screen%20Shot%202013-03-11%20at%2016.28.08.png)

## Usage

```html
<article>
  {% contact_list %}
</article>
```

**Note**: This tag is not configurable. 

You have to setup four models:

* ContactPerson
* Team
* Department
* Scope

**The ERM is**

![ERM](http://f.cl.ly/items/0C1H2Z1R3C1H0A212q3T/contact_list_erm.png)

### ContactPerson

| Attribute Name | Type        |
|:---------------|------------:|
| name           | String      |
| phone          | String      |
| email          | String      |
| department     | Reference to Department |
| photo          | Reference to Image     |
| team           | Reference to Team      |

Team is only necessary for a contact person that belongs to _Wohnungsverwaltung/Technik_


### Team 

| Attribute Name | Type        |
|:---------------|------------:|
| name           | String      |


### Department 

| Attribute Name | Type        |
|:---------------|------------:|
| name           | String      |


### Scope 

| Attribute Name | Type        |
|:---------------|------------:|
| street         | String      |
| team           | Reference to team |

