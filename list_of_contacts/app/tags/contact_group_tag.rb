module WordifyCms
  #
  # Renders a contact group (group might be a department or a team or a ...)
  # See list_of_contacts/app/assets/javascripts/list_of_contacts/controllers/list_of_contacts_controller
  # for more details
  #
  # Use like:
  #   {% contact_group %}
  #
  # @author Lars Müller

  class ContactGroupTag < Tag
    include ActionView::Helpers::SanitizeHelper
    register_tag "contact_group"

    def render(context)
      # fetch departmens and contacts
      group_id  = context['description']['group_id']
      group     = DbModelInstance.find(group_id)
      contacts  = context['models']

      street_klass = WordifyCms::DbModel.load(:name => "Scope")
      streets      = street_klass.scopes.select do |street|
        street.get_reference('team') == group
      end

      render_slim context, 
                  "/wordify_cms/templates/contact_group",
                  group:     group,
                  contacts:  contacts,
                  streets:   streets
    end

  end
end
