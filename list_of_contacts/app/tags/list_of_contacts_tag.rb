module WordifyCms
  class ListOfContactsTag < Tag
    include ActionView::Helpers::SanitizeHelper
    register_tag "contact_list"

    def render(context)
      # fetch departmens
      department_klass  = WordifyCms::DbModel.load(:name => "Department")
      departments       = department_klass.departments

      street_klass      = WordifyCms::DbModel.load(:name => "Scope")
      streets           = street_klass.scopes

      identifier        = "bereich_betriebskosten"

      running_costs_selector = lambda do |elem|
                                downcased = elem.get_attribute("name").downcase
                                downcased = strip_tags(downcased)
                                return downcased.gsub(/\s/, "_").eql?(identifier)
                              end
      department_objects = []
      departments_ = departments.sort_by(&:list_position).inject([]) do |result, elem|
        result << elem #unless running_costs_selector.call(elem)
        result
      end

      person_klass = WordifyCms::DbModel.load(:name => "ContactPerson")
      departments_.each do |department|
        contact_people = person_klass.contact_people.
                                      where("ref_department_id" => department.id)

        department_objects << {
          "department" => department.get_attribute("name"),
          "contact_people" => contact_people
        }
      end

      file_name         = "/wordify_cms/templates/contact_list"

      render_slim(context,
                  file_name,
                  {
                    :departments => department_objects,
                    :streets => streets
                  })
    end

  end
end
