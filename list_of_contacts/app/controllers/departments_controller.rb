module WordifyCms
  class DepartmentsController < WordifyController
    include ActionView::Helpers::SanitizeHelper
    layout false

    respond_to :json

    def get_for_scope
      scope = WordifyCms::DbModelInstance.find(params["id"])

      employees_team          = []
      team_scope              = scope.get_reference("team")
      person_scope            = scope.get_reference("person")
      facility_manager_scopes = scope.references.select do |reference|
        reference[:name].match(/facility_manager_(\d+)/)
      end
      employee_klass          = WordifyCms::DbModel.load(:name => "ContactPerson")

      if team_scope
        team_klass  = WordifyCms::DbModel.load(:name => "Team")
        team        = team_klass.teams.where(:id => team_scope.id)

        unless team.empty?
          team              = team.first
          # get all employees of this team
          employees_team    = employee_klass.contact_people.where("ref_team_id" => team.id)
        end
      end

      # Use pure Ruby to build hash insted of Rabl.
      employees = {}

      injector = lambda do |result, obj|
        node = {}
        [:email, :name, :phone].each do |attribute|
          if obj.respond_to?(attribute)
            node[attribute] = strip_tags(obj.get_attribute(attribute.to_s))
          end
        end

        img_ref = obj.get_reference("photo")
        node[:photo] = img_ref.to_liquid if img_ref.present?

        result << node
      end

      employees[:teams]       = employees_team.inject([], &injector)

      if person_scope
        employees_department = employee_klass.contact_people.
                                              where("id" => person_scope.id)
        employees[:departments] = employees_department.inject([], &injector)
      end

      unless facility_manager_scopes.blank?
        facility_manager_klass  =  WordifyCms::DbModel.
                                                load(:name => "FacilityManager")
        all_managers            =  facility_manager_klass.facility_managers

        facility_managers = all_managers.select do |manager|
          manager.id.in?(facility_manager_scopes.map{|fm| fm[:id]})
        end
        employees[:facility_managers] = facility_managers.inject([], &injector)
      end

      respond_with(employees) do |format|
        format.json { render :json => employees }
      end
    end

  end
end