#= require_tree ./templates
renderContactPeopleTemplate = ( contextTeams, \
                                contextDepartments,\
                                facility_managers) ->
  templateContext =
    contactPeopleTeams        : contextTeams
    contactPeopleDepartments  : contextDepartments
    facilityManagers          : facility_managers

  template = HandlebarsTemplates['contact_list'](templateContext)
  j("#placeholder_contact_people").html(template)

successFunc = (data, status, jqXHR) ->
  j("#contactPeopleTeams").empty()
  j("#contactPeopleDepartments").empty()
  j("#facilityManagers").empty()
  contactPeople = data
  unless contactPeople.hasOwnProperty("departments")
    contactPeople["departments"] = []
  unless contactPeople.hasOwnProperty("facility_managers")
    contactPeople["facility_managers"] = []

  renderContactPeopleTemplate(contactPeople.teams, \
                              contactPeople.departments, \
                              contactPeople.facility_managers)
  j('.departments-list').show()
  new WordifySearch.Highlighter({on: '.departments-list', jQuery: $})

errorFunc   = (jqXHR, status, errorThrown) ->


mtScopeChanged = (evt)->
  selectedScopeId = this.value
  if selectedScopeId?.length
    departmentId          = window.location.hash
    if departmentId
      window.location.hash  = departmentId. \
                              replace(/\=[\w]*/, "=#{selectedScopeId}")
    else
      window.location.hash = "department_id=#{selectedScopeId}"
    jQuery.ajax
      url: "/departments/for/scope/#{selectedScopeId}",
      type: 'GET',
      dataType: 'json',
      cache: false,
      success: successFunc,
      error: errorFunc
  else
    j('.departments-list').hide()


jQuery(document).ready ($)->
  qsParamRegex  = /\?[\w+]*/
  departmentId  = window.location.hash
  valueOfSelect = ""
  valueOfSelect = departmentId.split("=")[1] if departmentId?
  if valueOfSelect?
    if valueOfSelect.match(qsParamRegex)
      valueOfSelect = valueOfSelect.split(qsParamRegex)[0]
  $("#adress-select").val(valueOfSelect)
                     .on("change", mtScopeChanged)
                     .trigger("change")