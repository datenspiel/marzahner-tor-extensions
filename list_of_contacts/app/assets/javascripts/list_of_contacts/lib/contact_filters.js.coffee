return unless Wordify?

# Extends the Batman.Filter object by some useful functions
# see https://github.com/Shopify/batman/blob/master/src/view/filters.coffee
Wordify.ContactFilters =

  # use 'loc_' (list_of-contacts) to not accidently override any other method


  # Filter that returns a property of a virtual model
  # params:
  #   * model: Virtual model instance
  #   * prop:  String, name of the property (f.e. 'name')
  loc_virtualModelProperty: (model, prop, bindings)->
    # This only works here, not in a view because content isn't a Batman object
    val = model.get("content._#{prop}")
    # Remove html
    j(val).text()


_.extend Batman.Filters, Wordify.ContactFilters