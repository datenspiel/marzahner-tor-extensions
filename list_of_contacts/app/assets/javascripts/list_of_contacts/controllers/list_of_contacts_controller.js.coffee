return unless Wordify?

# Uses a Ruby WordifyCms::SortedSet Model.
# This is a more generic model that collects WordifyCms::DbModelInstances.
# (The SortedSet used with this controller collects contacts of a group, f.e.
# departments or teams)
#
# To make it work you have to create a ContentType with a specific identifier:
#   [js_controller]_[contact_model_name]_[group_model_name]
#
# For contacts of the same department use (2013-05-28):
#   [listOfContacts_ContactPerson_Department]
#
# For contacts of the same team use (2013-05-28):
#   [listOfContacts_ContactPerson_Team]
#
# author Lars Müller (Datenspiel GmbH)
#
class Wordify.ListOfContactsController extends Wordify.Controller

  # include controller concerns
  @mixin Wordify.Controllerable
  @mixin Wordify.ModalControllable

  # An identifier for this controller in the applications routes.
  routingKey: 'contacts'

  ###
  @Public
  ###
  createFromContent: (content, $container, model_names)->
    @set 'model', new Wordify.SortedSet(content: content)
    @edit $container, model_names


  ###
  @Public
  ###
  editFromContent: (content, $container)->
    Wordify.ContentType.find content.get('content_type_id'), (err, content_type)=>
      args = content_type.get('identifier').split('_')

      Wordify.SortedSet.find content.get('fine_body_id'), (err, model)=>
        @set 'model', model
        @edit $container, args[1..]

  ###
  @public
  ###
  edit: ($container, model_names)->
    # Only allow one edit at the time
    @getOrSet 'view', ()=>
      # Find all contacts
      @set 'all_contacts', Wordify.VirtualModel.loadInstances model_names[0]
      @observe 'all_contacts.length', @onGroupSelected
      # Create new Set for groups so we are able to bind to it
      @set 'groups', Wordify.VirtualModel.loadInstances model_names[1]
      # Set list models
      @set 'selected_contacts', new Wordify.ListModel
        source: dep_contacts = new Batman.Set
      @set 'available_contacts', new Wordify.ListModel
        source: new Batman.Set
        hidden: dep_contacts
      # Set default selected group
      @set 'selectedGroup', null
      @observe 'selectedGroup', @onGroupSelected
      @set 'selectedGroup', @get('model.description.group_id') || 0

      # Create the view
      view = new Batman.View
        source: 'contact_buckets/form'
        context: @_getRenderContext()

      # Add event listeners
      view.once 'ready',   ()=>

        @showModal view, $container
        containerId = $container.attr('id')
        j("##{containerId} .modal").addClass("dark bootstrap-wordify")

      view.once 'retired', ()=> @hideModal view, @get('model.content')
      # return it
      view


  ###
  @Public
  ###
  # Saves the current model
  save: (node, event, context)->
    model = context.get('model')
    # Remember the action
    action = if model.isNew() then 'created' else 'updated'
    # Save the model
    model.set 'description.group_id', @get('selectedGroup')
    model.set 'model_ids', @get('selected_contacts.source').mapToProperty('id')
    model.save (err) =>
      if err
        if err instanceof Batman.ErrorsSet
          alertify.error I18n.t('wordify_cms.notifications.content.errors.save')
        else
          @checkBanned err
      else
        alertify.success I18n.t("wordify_cms.notifications.content.#{action}")
        @retireView()

  ###
  @Public
  ###
  # Cancel editing a model
  cancelEdit: (node, event, context)->
    model = context.get('model')
    model.reset()
    @retireView()


  ###
  @public event listener
  # Called after the user has selected a group
  ###
  onGroupSelected: (new_Value)->
    ref_id = @get 'selectedGroup'
    available_contacts = @get('all_contacts').filter (item)->
      _(item.get("assigned_references")).any (ref)-> ref.id == ref_id
    selected_contacts = new Batman.Set

    # Each model_id to keep the order
    _(@get 'model.model_ids').each (id)->
      if item = available_contacts.find( (r)-> r.get('id') == id )
        selected_contacts.add(item)

    @get('available_contacts').replace available_contacts
    @get('selected_contacts').replace selected_contacts