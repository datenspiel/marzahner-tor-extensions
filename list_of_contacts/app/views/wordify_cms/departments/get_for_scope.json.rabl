collection @employees
node(:email) { |obj| strip_tags(obj.get_attribute("email")) }
node(:name) {  |obj| p obj.get_attribute("name");strip_tags(obj.get_attribute("name")) }
node(:phone) {  |obj| strip_tags(obj.get_attribute("phone")) }
node(:photo) do |obj|
  img_ref = obj.get_reference("photo")
  img_ref.to_liquid if img_ref.present?
end