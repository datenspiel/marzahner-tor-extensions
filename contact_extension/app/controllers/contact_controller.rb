module WordifyCms
  class ContactController < WordifyController
    layout false

    def send_contact_request
      location_to_redirect = params["location"]
      salutation = I18n.t('wordify_cms.contact_request.salutation.mrs')
      if params["mrs"].present?
        salutation = params["mrs"]
      elsif params["mr"].present?
        salutation = params["mr"]
      end
      mail_object = {
        "first_name"    => params["first-name"],
        "company"       => params["company"],
        "zipcode"       => params["zipcode"],
        "sender_email"  => params["email"],
        "last_name"     => params["last-name"],
        "street"        => params["street"],
        "phone"         => params["phone"],
        "topic_text"    => params["topic-text"],
        "salutation"    => salutation,
        "topic"         => params["topic"]
      }
      ContactRequestMailer.request_email(mail_object, 
                                         current_preferences.contact_email,
                                         request.host_with_port).deliver
      gon.contact_request_send = true
      redirect_to "#{location_to_redirect}?send=true"
    end

  end
end