module WordifyCms

  class ContactRequestMailer < ::ActionMailer::Base

    def request_email(obj,contact_mail,host)
      @params = obj
      @host = host
      recipients =  contact_mail
      subject = I18n.t('wordify_cms.contact_request.subject', :host => host)
      mail(:to => recipients,:subject => subject, 
           :template_name => ContactExtension::MAIL_TEMPLATE,
           :template_path => "wordify_cms/mailers",
           :from => @params["sender_email"])
    end
  end
end