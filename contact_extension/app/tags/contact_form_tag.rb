module WordifyCms
  class ContactFormTag < Tag

    def initialize(tag_name, params, token)
      @collection_context = args(params)
      @collection_class = @collection_context.singularize
      super
    end

    def render(context)
      params    = context.registers[:params]
      page      = context.registers[:page]
      site      = context.registers[:controller].current_site
      file_name = "/wordify_cms/templates/contact_form"
      topics    = get_instances

      if site.is_root?
        location = page.slug
      else
        location = "/#{site.title}#{page.slug}"
      end

      send_mail = params["send"].present? ? "true" : "false"

      render_slim(context,
                  file_name,
                  {:topics    => topics, 
                   :location  => location,
                   :send_mail => send_mail} )
    end

    private

    def get_instances
      vmodel_klass = WordifyCms::DbModel.load(:name => @collection_class.capitalize.camelize)
      return vmodel_klass.send(@collection_context)
    end

  end
end

Liquid::Template.register_tag('contact_form', WordifyCms::ContactFormTag)