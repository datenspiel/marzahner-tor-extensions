class ContactExtension < WordifyCms::Extension

  init_extension
  
  MAIL_TEMPLATE = "contact_request"

  config do |c|
    #c.extension_name = "contact_extension"
    c.add_routes do 
      match "/send/contact/request" => "contact#send_contact_request", 
            :as => :contact_request,
            :via => :post 
    end
  end
  
end