# Contact Extension

This extension basically adds a contact form liquid tag to the CMS and handling the mailing of this form to an email address that was defined in the CMS preferences. 

## Setup

Add a content or page type where you include in your template:

```html
{% contact_form 'topic_collection_name' %}
```

The argument is the pluralized name of the DbModel collection you have to define within the CMS. The 
DbModel should have a <code>description</code> attribute. 

## Next step

There is no next step. You're done. 

