# Arrow Link Extension

A very basic extension that provides a subclass of <code>Cms.ContentRenderers.LinkContent</code> to match the new content type implementation of Wordify.cms.
