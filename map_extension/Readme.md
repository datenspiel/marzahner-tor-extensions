# Map Extension

Adds a [Leaflet](http://leafletjs.com/) map to the page. 

![Leaflet.js map](http://f.cl.ly/items/3s0Q0B0H3Q38302A0q3Z/Screen%20Shot%202013-03-11%20at%2008.55.53.png)

### Install

Add 

```
gem "leaflet-rails", "~> 0.5.0"
```

to the Gemfile of the Rails app and run <code>bundle install</code>

### Usage

The map is configurable via the tag's options:

```html
{% map ' "lat" : "51.505" | "lng" : "-0.09" ' %}
```

which will set the map to the center of London.

There are some default options:

* <code>zoom</code>(level) (defaults to 13) 
* <code>maxZoom</code>(level) (defaults to 18)

You can set this in your tag definition:

```html
{% map ' "lat" : "51.505" | "lng" : "-0.09" | "zoom": "15" | "maxZoom": "17" ' %}
```
