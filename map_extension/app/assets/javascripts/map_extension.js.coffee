#= require 'leaflet'

mapLat      = j("#map").data("map-lat")
mapLng      = j("#map").data("map-lng")
mapZoom     = j("#map").data("map-zoom")
mapMaxZoom  = j("#map").data("max-zoom")

map = L.map('map').setView([mapLat, mapLng], mapZoom)
  
src = "http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png"
options = 
  maxZoom: mapMaxZoom
L.tileLayer(src,options).addTo(map)
L.marker([mapLat, mapLng]).addTo(map)

popup = L.popup()

#onMapClick = (e)->
#  popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn( map)

#map.on('click', onMapClick)