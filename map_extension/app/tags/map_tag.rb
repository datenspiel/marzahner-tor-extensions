module WordifyCms
  # Adds a Leaflet map to the page. 
  #
  # The map is configurable via the tag#s options:
  #
  # {% map ' "lat" : "51.505" | "lng" : "-0.09"' %}
  #
  # which will set the map to the center of London.
  #
  # There are some default options:
  # 
  # * zoom(level) (defaults to 13) 
  # * maxZoom(level) (defaults to 18)
  #
  # You can set this in your tag definition:
  #
  # {% map ' "lat" : "51.505" | "lng" : "-0.09" | "zoom": "15" | "maxZoom": "17" ' %}
  class MapTag < Tag

    def initialize(tag_name, params, token)
      @options = params
      strip_of_whitespace("options")
      jsonize_options
    end

    def render(context)
      @options["zoom"]     = 13 unless @options.has_key?("zoom")
      @options["maxZoom"]  = 18 unless @options.has_key?("maxZoom")
      file_name = "/wordify_cms/templates/map"
      render_slim(context,
                  file_name,
                  {
                    :lat => @options["lat"],
                    :lng => @options["lng"],
                    :zoom => @options["zoom"],
                    :maxZoom => @options["maxZoom"]
                  })
    end

    private 

    def jsonize_options
      options   = @options.split("|")
      @options  = json_to_hash.call(options) 
    end

  end
end

Liquid::Template.register_tag('map', WordifyCms::MapTag)