class ApSearch.Routers.ApartmentsRouter extends Backbone.Router
  
  routes:
    '':                             'overview'
    'apartments/:id/next':          'next'
    'apartments/:id(/:scroll_id)':  'show'


  ###
  @public
  ###
  initialize: ()->
    @main_view = new ApSearch.Views.Main(el: "#apartment-search")
    @inquiry_view = new ApSearch.Views.Inquiry(el: "#quick-apsearch")

  ###
  @public route
  ###
  overview: ()->
    showResults = ()=>
      overview = new ApSearch.Views.Overview(collection: @collection)
      @main_view.renderChildInto overview, '#apsearch-overview'
      @inquiry_view.setModel null
      @main_view.$el.fadeIn(400)

    if @collection == undefined
      @collection = null
      @main_view.submit()
    else if j('#apsearch-overview').length
      showResults()
    else
      @main_view.$el.fadeOut 400, ()=>
        @main_view.reset()
        showResults()

  ###
  @public route
  ###
  show: (id, scroll_id)->
    @_withCollection '', ()=>
      if model = @collection.get(id)
        view = new ApSearch.Views.Expose(model: model)
        @main_view.$el.fadeOut 400, ()=>
          @main_view.empty().appendChild view
          @inquiry_view.setModel model
          @main_view.$el.fadeIn 400, ()->
            view.initMap() # needs to be done AFTER making visible in the DOM
            view.scrollTo scroll_id

  ###
  @public route
  ###
  next: (id)->
    if @collection and (model = @collection.get(id))
      idx = @collection.indexOf(model)
      model = @collection.at(idx + 1)
    fragment = if model? then "apartments/#{model.id}" else ''
    @navigate fragment, replace: true, trigger: true

  ###
  @public
  ###
  search: (data)->
    if data?
      @collection = null
      @_withCollection data, ()=> Backbone.history.loadUrl '' # load or reload

  ###
  @private
  ###
  # Loads the apartments into selfs collection for the given data
  _withCollection: (data, callback)->
    if @collection
      callback()
    else  
      ApSearch.Collections.Apartments.search data, (err, apartments)=>
        @collection = apartments
        callback()