#= require leaflet
#= require_self
#= require_tree ./support
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./routers
#= require_tree ./views

# Create namespace
window.ApSearch =
  Support:      {}
  Models:       {}
  Collections:  {}
  Routers:      {}
  Views:        {}

# Register Handlebar I18n Helper
Handlebars.registerHelper 'I18n', (str, options)->
  new Handlebars.SafeString(I18n.t str, options.hash)


# Entry point to the apartment search client side javascript
j ->
  if j('#apartment-search').length
    window.ApSearch.router = new ApSearch.Routers.ApartmentsRouter()
    Backbone.history.start()