###
Client-side model representation of WordifyCms::Apartment. 
###
class ApSearch.Models.Apartment extends Backbone.Model

  idAttribute: "_id"

  ###
  @public
  ###
  # Overrides toJSON method to add some extra properties
  toJSON: ()->
    answ = super arguments...
    # Add virtual properties
    answ.address_one_line = @addressOneLine()
    answ.address_html     = @addressHtml()
    answ.address_street   = @addressStreet()
    answ.address_city     = @addressCity()
    # Parse each property and return
    answ[key] = @_parseJSON(key, val) for key, val of answ
    answ


  ###
  @public
  ###
  addressStreet: ()->
    "#{@get('street')} #{@get('street_nr')}"

  ###
  @public
  ###
  addressCity: ()->
    "#{@get('postcode')} #{@get('city')}"

  ###
  @public
  ###
  addressOneLine: ()->
    "#{@addressStreet()}, #{@addressCity()}"

  ###
  @public
  ###
  addressHtml: ()->
    "#{@addressStreet()}<br/>#{@addressCity()}"


  ###
  @private
  ###
  _parseJSON: (key, val)->
    switch key
      when 'space'
        I18n.toNumber(val, precision: 2)
      when 'rent', 'additional_costs', 'cost_of_heating', 'cost_of_water', 'total_rent'
        I18n.l('currency', val)
      else
        new Handlebars.SafeString(val || '')
