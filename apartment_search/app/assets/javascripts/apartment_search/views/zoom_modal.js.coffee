#= require_tree ../templates

class ApSearch.Views.ZoomModal extends ApSearch.Support.CompositeView

  template: HandlebarsTemplates['apartment_search/templates/zoom_modal']

  className: 'modal hide'

  ###
  @public
  ###
  render: ()->
    @empty().$el.html @template(img_src: @options.img_src)
    # Make modal as big as the image
    @$el.css width: 'auto'
    @$('.modal-body').css 'max-height': 'none'
    this


  ###
  @public
  ###
  show: ()->
    j('body').append(@$el)
    @$el.modal 'show'

  ###
  @public
  ###
  hide: ()->
    @$el.modal 'hide'
    @remove()
