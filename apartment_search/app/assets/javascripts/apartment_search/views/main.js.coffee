class ApSearch.Views.Main extends ApSearch.Support.CompositeView

  events:
    "submit form": "_onSubmitClick"


  initialize: ()->
    super arguments...
    @_origin = @$el.html()

  ###
  @public
  ###
  # Resets the content to the origin html string
  reset: ()->
    @empty().$el.html @_origin


  # Return serialized form data if it has any data, null otherwise
  serialize: ()->
    # Check if the form has any data
    inputs = @$("form input")
    has_data = _(inputs.toArray()).any (input)-> @$(input).val() != ''
    # Return form data or null
    if has_data then inputs.serialize() else null


  ###
  @public
  ###
  # Submits the form if it has data
  submit: ()->
    ApSearch.router.search @serialize()


  ###
  @private
  ###
  _onSubmitClick: (evt)=>
    evt.preventDefault()
    @submit()
