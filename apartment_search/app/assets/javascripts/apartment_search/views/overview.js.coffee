#= require_tree ../templates

class ApSearch.Views.Overview extends ApSearch.Support.CompositeView

  template: HandlebarsTemplates['apartment_search/templates/overview']

  render: ()->
    @empty().$el.html @template(apartments: @collection.toJSON())
    this
