#= require_tree ../templates

class ApSearch.Views.Expose extends ApSearch.Support.CompositeView

  template:  HandlebarsTemplates['apartment_search/templates/expose']

  events:
    'click a.maplink':  '_onMapLinkClick'
    'click a.zoom':     '_onZoomClick'

  ###
  @public
  ###
  render: ()->
    @empty().$el.html @template
      apartment: @model.toJSON()
      pictures:  @model.get('pictures')
      location:  location.href
    # Init carousel and map
    @_initCarousel()
    this

  ###
  @public
  ###
  scrollTo: (id)->
    j('html, body').animate({scrollTop: @$("##{id}").offset().top}, 500) if id?


  ###
  @public
  ###
  initMap: ()->
    coordinates = _.clone(@model.get('coordinates') || []).reverse()
    zoom        = 13 # 0..18
    # Create the map
    map = L.map('expose-map').setView(coordinates, zoom)
    # Add the layers
    L.tileLayer("http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png")
      .addTo(map)
    # Add the marker
    marker = L.marker(coordinates)
      .addTo(map)
      .bindPopup("<b>#{@model.addressStreet()}</b><br>#{@model.addressCity()}.").openPopup()


  ###
  @private
  ###
  _initCarousel: ()->
    $carousel = @$('#expose-carousel')
    $carousel.find('.carousel-inner .item').first().addClass('active')
    $carousel.find('.carousel-indicators li').first().addClass('active')
    $carousel.carousel $carousel.data()


  ###
  @private event
  ###
  _onMapLinkClick: (evt)->
    evt.preventDefault()
    @scrollTo 'expose-map'

  ###
  @private event
  ###
  _onZoomClick: (evt)->
    evt.preventDefault()
    img_src = @$('#expose-carousel .carousel-inner .item.active img').attr('src')
    view = new ApSearch.Views.ZoomModal(img_src: img_src)
    view.render().show()