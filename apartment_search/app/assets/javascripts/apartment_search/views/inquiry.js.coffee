#= require_tree ../templates

class ApSearch.Views.Inquiry extends ApSearch.Support.CompositeView

  template: HandlebarsTemplates['apartment_search/templates/inquiry']

  events:
    'submit form':  'submit'

  ###
  @public
  ###
  initialize: ()->
    # autorender
    @render()

  ###
  @public
  ###
  render: ()->
    @empty().$el.html @template(apartment: @model?.toJSON())
    if @model then @$el.fadeIn(400) else @$el.fadeOut(400)
    this


  ###
  @public
  ###
  # Updates the model
  setModel: (model)->
    if @model != model
      @model = model
      @render()

  ###
  @public
  ###
  # Return serialized form data if it has any data, null otherwise
  serialize: ()->
    @$("form input, form textarea").serialize()

  ###
  @public
  ###
  # Submits the form if it has data
  submit: (evt)->
    evt.preventDefault()

    # Internal callback that is executed after successful ajax request
    handleResponse = (data)=>
      data ?= {}
      if data.success
        @_showMessages [I18n.t 'wordify_cms.apartments.inquiry.success']
        @$("form")[0].reset()
      else if data.errors?
        @_showMessages [data.errors[0]]
      else
        @_showMessages [I18n.t 'wordify_cms.apartments.inquiry.errors.general']

    # Internal callback that is executed when ajax request had an error
    onError = (jqXHR)=>
      if jqXHR.status == 422
        handleResponse j.parseJSON(jqXHR.responseText)
      else
        handleResponse()
    
    # Ajax post request    
    j.ajax
      type:     "POST"
      url:      "/apartments/#{@model.id}/inquiry"
      data:     @serialize()
      success:  handleResponse
      error:    onError
      dataType: 'json'

  ###
  @private
  ###
  _showMessages: (messages)=>
    messages ?= []
    if messages.length
      clearTimeout(@timer) if @timer # reset timer
      @$('#inquiry-errors h2').html messages.join('<br>')
      @$('#inquiry-errors').fadeIn 500
      @timer = setTimeout(@_hideMessages, 8000)

  ###
  @private
  ###
  _hideMessages: ()=>
    @$('#inquiry-errors').fadeOut(500)