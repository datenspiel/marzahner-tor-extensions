###
This code was taken from https://github.com/thoughtbot/backbone-support
Thanks for this great piece of code but I had some problems with it.
F.e. method appendChildTo sometimes worked and sometimes it doesn't.
However it works when implementing the code myself. (No time to figure out why)
###
# @author Lars Mueller
class ApSearch.Support.CompositeView extends Backbone.View
    
  constructor: (options)->
    @children = []
    super(options)

  leave: ()->
    @trigger('leave')
    @remove() # removes from DOM and calls stopListening()
    @_leaveChildren()
    @_removeFromParent()

  renderChild: (view)->
    view.render()
    @children.push(view)
    view.parent = this
    this
  
  renderChildInto: (view, container)->
    @renderChild(view)
    @$(container).empty().append(view.el)
    this

  appendChild: (view)->
    @renderChild(view)
    @$el.append(view.el)
    this
  
  appendChildTo: (view, container)->
    @renderChild(view)
    @$(container).append(view.el)
    this
  
  prependChild: (view)->
    @renderChild(view)
    @$el.prepend(view.el)
    this
  
  prependChildTo: (view, container)->
    @renderChild(view)
    @$(container).prepend(view.el)
    this

  empty: ->
    @_leaveChildren()
    @$el.empty()
    this

  _leaveChildren: ()->
    # Iterate over clone because contents of @children will change due iteration 
    view.leave?() for view in _.clone(@children)

  _removeFromParent: ()->
    @parent?._removeChild(this)

  _removeChild: (view)->
    index = @children.indexOf(view)
    @children.splice(index, 1)
