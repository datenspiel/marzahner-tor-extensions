class ApSearch.Collections.Apartments extends Backbone.Collection
  model: ApSearch.Models.Apartment


  # Runs a server side search with the given params and creates a new
  # apartments collection with the results
  #
  # params:   Object/String - will get passed to the jQuery ajax function
  # callback: Function - (err, models)->
  @search: (params, callback)->
    # Internal callback that is executed after successful ajax request
    onSuccess = (data)->
      arr = _(data).map (item)-> new ApSearch.Models.Apartment(item)
      callback null, new ApSearch.Collections.Apartments(arr)

    # Internal callback that is executed when ajax request had an error
    onError = ()->
      callback 'An error occured'

    # Ajax post request    
    j.ajax
      type:     "POST"
      url:      "/apartments/search"
      data:     params
      success:  onSuccess
      error:    onError
      dataType: 'json'
