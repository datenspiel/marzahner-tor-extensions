module WordifyCms
  class Apartment
    include ::Mongoid::Document

    class << self

      # Finds apartments by a given options hash.
      #
      # options example:
      #   {
      #     "rooms" => { "gte" => "3", "lte" => "4" },
      #     "rent"  => { "lte" => "480" },
      #     "space" => {},
      #     "floor" => { "eq" => "5" }
      #   }
      #
      def search(options)
        # Internal lambda that combines an array of conditions with a given
        # command, usually '$and' or '$or'
        combine_conditions = ->(cmd, arr) do
          arr.blank? ? {} : ( arr.length == 1 ? arr[0] : { cmd => arr } )
        end
        # Internal lambda that creates the conditions for one attribute
        attr_conditions = ->(args) do
          name, old, terms = args[0], args[1], {}
          # Prepend a '$'-sign to each key and convert each value to a number
          old.keep_if { |k,v| v.present? }.each { |k,v| terms["$#{k}"] = v.to_f }
          # Return nil if no conditions were found
          return nil if terms.blank?
          # Check for equality only (nothing else) if terms has key '$eq'
          terms = terms['$eq'] || terms

          # Replace 'lte' with 'lt'
          # So instead of searching for 'x <= 2' it searches for 'x < 3'
          # This way we also get apartments with 1.1 rooms when looking for
          # apartments with max 1 room
          if terms['$lte']
            terms['$lt'] = terms['$lte'] + 1
            terms.delete('$lte')
          end

          # If conditions were found also select data having
          # no value for that field
          combine_conditions.call('$or', [
            { name => terms },
            { name => {'$exists' => false} }
          ])
        end

        conditions = (options || {}).map(&attr_conditions).compact
        apartments = WordifyCms::Apartment.unscoped
        apartments.selector.merge! combine_conditions.call("$and", conditions)
        #p apartments.selector
        #p apartments.count
        apartments
      end
    end # <<< class


    # Returns an array of hashes {url: ..., footprint: ...} for pictures
    # of this apartment
    # Pictures should be available at /public/apartments/
    def pictures
      (1..6).inject([]) do |result, nr|
        filename = self.send("pic0#{nr}")
        description = self.send("pic_description0#{nr}")
        if filename.present?
          result << { url:        "/vendor/img/apartments/#{filename.downcase}",
                      footprint:  description == "Grundriss" }
        end
        result
      end
    end

    # Returns all footprint pictures
    def pictures_only
      pictures.select { |pic| !pic[:footprint] }
    end

    # Returns all pictures except footprints
    def footprints
      pictures.select { |pic| pic[:footprint] }
    end


    # Override method to not throw an exception
    # Because fields are not defined there might be apartments that
    # respond f.e. to floor and others don't
    def method_missing(method_id, *args, &block)
      super rescue nil
    end
  end
end