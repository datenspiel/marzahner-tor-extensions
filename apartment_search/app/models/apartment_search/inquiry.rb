# Codebase: http://matharvard.ca/posts/2011/aug/22/contact-form-in-rails-3/

module WordifyCms
  module ApartmentSearch
    
    class Inquiry

      include ActiveModel::Validations
      include ActiveModel::Conversion
      extend ActiveModel::Naming

      attr_accessor :name, :email, :fon, :message #:appointment

      validates :name, :email, presence: true
      validates :email, format: { with: %r{.+@.+\..+} }
      
      def initialize(attributes = {})
        attributes.each do |name, value|
          send("#{name}=", value)
        end
      end

      def persisted?
        false
      end


      def appointment
        @appointment
      end

      def appointment=(val)
        @appointment = 'on'.eql?(val)
      end
    
    end

  end
end