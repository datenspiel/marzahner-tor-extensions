collection @apartments
attributes :_id, :ident, :rooms, :space, :floor,
           :street, :street_nr, :postcode, :city, :district,
           :year_of_construction, :characteristics,
           :available_at, :neighborhood,
           :rent, :additional_costs, :cost_of_heating, :cost_of_water,
           :total_rent, :admission_fee,
           :contact_name, :contact_fon, :contact_email,
           :pictures, :coordinates