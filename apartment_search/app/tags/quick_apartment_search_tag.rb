module WordifyCms
  
  # Use like this:
  #
  #   {% quick_apartment_search /wohnungssuche %}
  #
  # Pass the route to the page that contains the apartment_search tag.
  #
  class QuickApartmentSearchTag < Tag

    register_tag "quick_apartment_search"

    def initialize(tag_name, action, tokens)
      super
      @action = action.strip
    end


    def render(context) 
      file_name = "/wordify_cms/templates/quick_apartment_search"
      render_slim context, file_name, action: @action
    end

  end
end
