module WordifyCms
  class ApartmentSearchTag < Tag

    register_tag "apartment_search"

    def render(context)
      # Extract search params
      search_params = context.registers[:params]['apartment'] || {}
      # Do render
      render_slim context,
                  "/wordify_cms/templates/apartment_search",
                  apartment: default_values.merge(search_params)
    end


    # Returns default values for template
    # This is necassary to not get a 'nil' error when doing f.e.
    # "#{ apartment['rooms']['gte'] }"
    def default_values
      {
        'rooms' => { 'gte' => '', 'lte' => '' },
        'rent'  => { 'gte' => '', 'lte' => '' },
        'space' => { 'gte' => '', 'lte' => '' },
        'floor' => { 'eq' => '' }
      }
    end

  end
end
