module WordifyCms
  class ApartmentsController < WordifyController 
    layout false

    respond_to :json, except: :print
    respond_to :html, only:   :print


    before_filter :load_resource, except: :search


    # Public route
    #    POST /apartments/search
    #
    # Finds apartments by given search params 'apartment'
    def search
      @apartments = Apartment.search(params['apartment'])
      respond_with @apartments
    end

    # Public route
    #    GET /apartments/:id/print
    #
    # Render the expose print for the given apartment id
    def print
      respond_with @apartment
    end


    # Public route
    #    POST /apartments/:id/inquiry
    #
    # Sends an email to the apartments contact about a prospect
    def inquiry
      @inquiry = ApartmentSearch::Inquiry.new(params['inquiry'])
      
      if @inquiry.valid?
        ApartmentMailer.new_message(@apartment, @inquiry).deliver
        ApartmentMailer.confirmation_message(@apartment, @inquiry).deliver
        res = { json: { success: true }, status: 200 }
      else
        errors = @inquiry.errors.full_messages
        res = { json: { errors: errors }, status: 422 }
      end

      respond_to do |format|
        format.json { render res }
      end


      #/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

      #errors = []
      #res = { json: {}, status: 200 }
      ## Validations
      #if inquiry_params['email'].blank?
      #  if inquiry_params['fon'].blank?
      #    errors << I18n.t('wordify_cms.apartments.inquiry.errors.emailfon_blank')
      #  end
      #  errors << I18n.t('wordify_cms.apartments.inquiry.errors.email_format')
      #end
      ## Build response
      #if res[:json][:success] = errors.empty? 
      #  ApartmentMailer.inquiry(@apartment, inquiry_params).deliver
      #else
      #  res[:json][:errors] = errors
      #  res[:status] = 422
      #end
      ## Respond
      #respond_to do |format|
      #  format.json { render res }
      #end
    end


private

    # Loads the apartment by the given id
    def load_resource
      @apartment = Apartment.find(params['id'])
    end

  end 
end