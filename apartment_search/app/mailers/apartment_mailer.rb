#encoding: utf-8

module WordifyCms
  class ApartmentMailer < ::ActionMailer::Base

    default from: "noreply@marzahner-tor.de"


    # Public: Sends a message to "Marzahner Tor"
    def new_message(apartment, inquiry)
      @inquiry = inquiry

      options = {
        to:      apartment.contact_email,
        subject: "Anfrage für Wohnung #{apartment.ident}"
      }
      mail(options)
    end


    # Public: Sends a confirmation message to the sender
    def confirmation_message(apartment, inquiry)
      if inquiry.email.present?
        @inquiry = inquiry

        options = {
          to:      inquiry.email,
          subject: "Wohnungsgenossenschaft Marzahner Tor: Anfrage für Wohnung #{apartment.ident}"
        }
        mail(options)
      end
    end
  end
end
