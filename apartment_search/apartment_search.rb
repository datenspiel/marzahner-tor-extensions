require 'leaflet-rails'

class ApartmentSearch < WordifyCms::Extension

  register_extension

  config do |c|
    c.add_routes do
      post  "/apartments/search"      => "apartments#search"
      post  "/apartments/:id/inquiry" => "apartments#inquiry"
      get   "/apartments/:id/print"   => "apartments#print"
    end

  end

end