# Apartment search extension #
Last updated: 2013-06-26

Some quick notes about the apartment search extension:

## Import script
**Will destroy all apartments in the database without any further confirmation!!**

   1.  Update database settings in <code>config/mongoid.yml</code>
   2.  Update FTP settings in <code>config/ftp.json</code>
   3.  For notifying by email adjust settings in <code>config/email.json</code>
   4.  Copy <code>run.sh.sample</code> to <code>run.sh</code> and adjust the path settings.
   5.  Call the script with <code>sh run.sh</code>.

## Liquid tags
You can use the apartment-search tag without the quick-apartment-search tag but
not the other way around.

### quick-apartment-search-tag: ###
This tag does nothing but redirect to the page with the apartment-search tag
passing the given parameters along.

**Use like this:**

    <section class="block box">
        <h3 class="box__heading">Wohnungssuche</h3>
        {% quick_apartment_search /wohnungssuche %}
    </section>

First parameter for the liquid tag is the relative url of the page that
contains the apartment-search-tag

### apartment-search-tag: ###
This tag runs the search with the given parameters using AJAX and displays the results.

**Use like this:**

    <section class="block">
        <h1>Wohnungssuche</h1>
        <p class="lead">Hier können Sie schnell und problemlos nach der passenden Wohnung suchen.</p>
        {% apartment_search %}
    </section>
