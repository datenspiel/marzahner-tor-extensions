require 'rubygems'
require 'bundler/setup'

Bundler.require
require_relative "lib/ftp"

RAILS_REQUIERED_INFO  = %Q{You must specify the path to the rails root}
PATH_REQUIRED_INFO    = %Q{You must specify an import path}

<<-DOC
  Usage:

  ruby import.rb --path PATH/TO/IMPORT/DIR --rails-root PATH_TO_PUBLIC_DIR \
  --notify=true
DOC

opts = Trollop::options do
  banner <<-EOS
    'Import' is a little script to fetch a 'Wodis Sigma' generated zip file
    from FTP and extract data to use it within the Marzahner Tor Rails app.

    Usage:
           ruby import.rb [options]
    where [options] are:
  EOS
  opt :path, "The path saving the csv and image data", :type => :string
  opt :rails_root, "The Rails application path", :type => :string
  opt :env, "The Rails environment", :type => :string, :default => "development"
  opt :notify, "Enables/disables email notification on failure"
  opt :tweet_on_success, "Tweet a short message on success.", :type => :boolean,
                                                              :default => false
end

# Setup mongodb connection
ENV['MONGOID_ENV'] = opts[:env]
Mongoid.load! File.expand_path(File.join(File.dirname(__FILE__),"config","mongoid.yml"))

Trollop::die :path,PATH_REQUIRED_INFO unless opts[:path]
Trollop::die :rails_root, RAILS_REQUIERED_INFO unless opts[:rails_root]

# Run the data fetching from ftp and afterwards importer
WordifyCms::FlatDataFetcher.run(:path         => opts[:path],
                                :rails_root   => opts[:rails_root],
                                :notify_email => opts[:notify],
                                :tweet_on_success => opts[:tweet_on_success])