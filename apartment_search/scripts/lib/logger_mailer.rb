require_relative 'config_reader'

module WordifyCms

  # Public: Send logging notifications to recipients configured in
  # config/email.json.
  #
  class LoggerMailer

    # Public: Notifies by email with a given error message.
    #
    # Inits a new instance of LoggerMailer and calls #notify.
    #
    # notify_message - The message used in the notifiers body.
    #
    def self.notify(notify_message)
      self.new.notify(notify_message)
    end

    # Public: Notifies by email with a given error message.
    #
    # notify_message - The message used in the notifiers body.
    #
    def notify(notify_message)
      Pony.mail(:to => recipients,
                :via => :sendmail,
                :subject => subject,
                :body => "#{Time.now} - #{notify_message}",
                :from => sender

               )
    end

    private

    def sender
      ConfigReader.read("email.json")["from"]
    end

    def recipients
      ConfigReader.read("email.json")["recipients"]
    end

    def subject
      ConfigReader.read("email.json")["subject"]
    end

  end

end