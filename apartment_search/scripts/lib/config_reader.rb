module WordifyCms

  module ConfigReader
    extend self

    # Public: Reads a JSON file located in ../config.
    #
    # json_file_name - the JSON file name.
    #
    # Returns the JSON file content as Hash.
    def read(json_file_name)
      json = File.read(File.join(File.dirname(__FILE__), "..","config", json_file_name))
      return MultiJson.load(json)
    end

  end

end