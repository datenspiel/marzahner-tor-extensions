require_relative "logger_mailer"

module WordifyCms
  module Die

    def die(abort_message, options={})
      if options[:notify_by_mail]
        LoggerMailer.notify(abort_message)
      end
      abort(abort_message)
    end

  end
end