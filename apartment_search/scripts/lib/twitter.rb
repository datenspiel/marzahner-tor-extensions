require 'twitter'

%w{ core_ext config_reader }.each{ |file| require_relative file }

module WordifyCms
  class Twitter

    def self.tweet(apartment_count, time_of_import)
      self.new.tweet(apartment_count,time_of_import)
    end

    def initialize
      setup_twitter
    end

    def tweet(count, time_of_import)
      flats       = count.eql?(1) ? "flat" : "flats"
      pretty_time = time_of_import.to_pretty
      tweet = "Hey, @#{@twitter_config["at_user"]}, I imported #{count} #{flats} #{pretty_time} at the mysterious time of #{time_of_import.to_i}!"
      ::Twitter.update(tweet)
    end

    def setup_twitter
      @twitter_config = ConfigReader.read("twitter.json")
      ::Twitter.configure do |config|
        config.consumer_key       = @twitter_config["consumer_key"]
        config.consumer_secret    = @twitter_config["consumer_secret"]
        config.oauth_token        = @twitter_config["oauth_token"]
        config.oauth_token_secret = @twitter_config["oauth_token_secret"]
      end
    end

    private :setup_twitter
  end
end