module WordifyCms
  # Simple helper class that is needed for the import
  # Uses geocoder gem to store lat and lng of an apartment
  class Apartment
    include Mongoid::Document
    include Geocoder::Model::Mongoid
    include Mongoid::Timestamps

    field :coordinates, type: Array
    geocoded_by :address
    # There is just a small number of apartments. If it gets more put this
    # in a background job
    after_validation :geocode

    def address
      # Use attributes to not get a method missing exception if a field was empty
      "#{attributes['street']} #{attributes['street_nr']}, #{attributes['postcode']} #{attributes['city']}"
    end
  end
end