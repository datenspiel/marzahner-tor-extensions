require 'net/ftp'

%w{ importer die config_reader }.each{ |file| require_relative file }

module WordifyCms

  class FlatDataFetcher
    include Die

    # Public: Starts downloading and importing of apartments
    #
    # opts - A options Hash to define the environment.
    #   :path - The path to save the zip file to
    #   :rails_root - The path to extract the data
    #
    def self.run(opts)
      self.new.load(opts)
    end


    # Public: Loads zip data from a remote file.
    #
    # opts - A options Hash to define the environment.
    #   :path - The path to save the zip file to
    #   :rails_root - The path to extract the data
    def load(opts)
      @path       = opts[:path]
      @rails_root = opts[:rails_root]
      @notify     = opts[:notify_email]
      @tweet      = opts[:tweet_on_success]
      create_dir(@path)
      retrieve_from_ftp
    end

    private

    def create_dir(dir)
      FileUtils.mkdir_p(dir) unless File.exists?(dir)
    end


    def retrieve_from_ftp
      login, pwd, host = ftp_data
      ftp  = Net::FTP.new(host, login, pwd)
      begin
        file = ftp.nlst("*.zip").sort_by{|f| ftp.mtime(f)}.reverse.first
        retrieve(file, :ftp_handler => ftp)
      rescue Net::FTPTempError => fte
        die("Died - No files found.", :notify_by_mail => @notify)
      end
    end

    def retrieve(file, options={})
      ftp       = options[:ftp_handler]
      save_path = File.join(@path, File.basename(file))
      ftp.getbinaryfile(file, save_path)
      ftp.close
      if File.exists?(save_path)
        WordifyCms::Importer.run(:working_dir => @path,
                                 :rails_root  => @rails_root,
                                 :tweet       => @tweet)
      else
        # die without importing
        die("Died - Download of file was not successful ...",
            :notify_by_mail => @notify)
      end
    end

    def ftp_data
      ftp_data = ConfigReader.read("ftp.json")["ftp"]
      return ftp_data["login"], ftp_data["pwd"], ftp_data["host"]
    end

  end

end