require 'csv'
require 'zip/zip'
require 'fileutils'

%w{ config_reader apartment twitter}.each {|file| require_relative file }


module WordifyCms

  class Importer
    class << self

      #CSV_LOOKUP_PATH = File.expand_path(File.join(File.dirname(__FILE__), "..","config","lookup.json"))

      # Starts the import process
      def run(options)
        @working_dir    = options[:working_dir]
        @rails_root     = options[:rails_root]
        @tweet_enabled  = options[:tweet]
        comment         = 'error' 

        if f_zip = zip_file

          begin
            unzip f_zip

            if f_csv = csv_file
              # move images to rails app
              move_files images, File.join(@rails_root, "public", "vendor", "img", "apartments")
              # import csv file
              import_csv f_csv
              comment = 'done'
            end
          ensure
            thumbs = File.join(@working_dir,"thumbs.db")
            # Remove images csv and thumbs.db
            ([f_csv, thumbs] + images).each { |f| remove_file f }
            # Rename zip file
            FileUtils.mv f_zip, "#{f_zip}.#{comment}"
          end
      
        end
      end


      # Returns the latest zip file found in the working directory
      # or nil if there is none
      def zip_file
        Dir.glob(File.join(@working_dir, "*.zip")).max_by { |f| File.mtime(f) }
      end

      # Returns the latest csv file found in the working directory
      # or nil if there is none
      def csv_file
        Dir.glob(File.join(@working_dir, "*.csv")).max_by { |f| File.mtime(f) }
      end

      # Returns all images in the working directory
      def images
        Dir[File.join(@working_dir, "*.{jpg,png,gif,jpeg}")]
      end

# instance methods here.
private

      # Unzips a given file to the working directory
      def unzip(file, target=@working_dir)
        Zip::ZipFile.open(file) do |zip_file|
          zip_file.each do |f|
            f_path = File.join target, f.name.downcase
            zip_file.extract f, f_path
          end
        end
      end

      # Moves given files to a given directory
      def move_files(files, target_folder)
        files.each do |f|
           FileUtils.mv f, File.join(target_folder, f.split('/').last.downcase)
        end
      end

      # Removes a given file
      def remove_file(file)
        FileUtils.rm file if file and File.exists?(file)
      end

      # Imports the given csv file into the db
      def import_csv(file)
        # Destroy all apartments
        WordifyCms::Apartment.destroy_all
        # Load the lookup file
        lookup_tbl = ConfigReader.read("lookup.json")["format"]
        # Add apartments from csv
        File.open(file, "r:ISO-8859-1:utf-8") do |f|
          CSV.parse(f, col_sep: '|') do |row|
            # Create an apartment for each csv row
            apartment = WordifyCms::Apartment.new
            # Store the value for each cell atomically in DB
            row.each_with_index do |cell, idx|
              # Find cell definition WITH the given index starting at 1
              cell_def = lookup_tbl.find { |cd| cd['index'] == idx + 1 }
              #p "#{cell_def['name']} -> #{cell}" if cell_def
              # Lookup attr_name for the cell
              attr_name = cell_def["attr_name"] unless cell_def.nil?
              # Set the value when conditions are given
              unless cell.nil? or attr_name.nil?
                # Convert cell to known types
                val = case cell_def['type']
                  when 'Integer' then cell.to_i
                  when 'Float'   then cell.gsub(',', '.').to_f
                  else
                    cell.force_encoding("UTF-8")
                end
                # Set the value
                apartment.set attr_name, val
              end
            end # row.each
            apartment.save
          end # CSV.parse
        end # File.open
        p "#{Apartment.count} apartments imported!"
        Twitter.tweet(Apartment.count,Time.now) if @tweet_enabled
      end

    end
  end
end
