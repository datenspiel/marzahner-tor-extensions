## marzahner-tor-extensions

Extensions are located within the Rails application that includes the 
CMS. 

Any extension should have its own directory in 
Rails.root/lib/wordify.cms/extensions. 

The default extension structure is:

extension-name

  * app
    * controllers
    * mailers
    * models
    * views
    * tags
  * confg
  * lib
    
It's basically very simliar to a Rails Engine or Rails App.

The extension file is located at the root level of the extension should be 
named in the same way the extension folder is named.

Example:

```rb
module WordifyCms
 class AwesomeExtension < Extension
    
   # This line is important. It initializes the view, mailer and I18n paths.
   init_extension

   # Configure your extension by adding some routes or a custom extension name
   config do |c|
      c.add_routes do 
        match "/send/contact/request" => "contact#send_contact_request", 
                                          :as => :contact_request,
                                          :via => :post 
      end
      c.extension_name = "hurrah" # assumes your extension folder is called 'hurrah'
    end
 end
end
```

## Installation

Add it to your Rails app via a git submodule 

```
git submodule add git@github.com:datenspiel/marzahner-tor-extensions.git lib/wordify.cms/extensions
git submodule init
git submodule update
```