# Lodgers club extension

Adds a list of downloadable lodgers club issues. 

## Usage

You can define a sorting to the tag:

* desc
* asc

where <code>asc</code> is default.

```html
<article>
  {% club_issues_list ' "css" : "magazine-list" | "sort": "desc"' %}
</article>
```

**Note**: 

The lists css class is configurable with <code>css</code>.

If no option is given <code>issue-list</code> is used. 

You have to setup a model "Magazine" with the following attributes:

 Attribute Name | Type        |
|:---------------|------------:|
| title          | String      |
| pdf_link       | External Link      |
| image          | Reference to Image     |
| published_at   | Date |

If <code>published_at</code> is in the future, no issue will displayed.
