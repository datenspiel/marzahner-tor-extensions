module WordifyCms

  class LodgersClubIssueTag < Tag
    register_tag "club_issues_list"

    def initialize(tag_name, params, tokens)
      args(params)
      super
    end

    def render(context)
      magazine_klass = WordifyCms::DbModel.load(:name => "Magazine")
      magazines      = magazine_klass.magazines

      magazines = magazines.sort_by(&:_published_at)
      magazines.delete_if{|issues| issues.published_at.to_i > Time.now.to_i}
      if @args.has_key?("sort")
        magazines.reverse! if @args["sort"].eql?("desc")
      end

      css            = @args.has_key?("css") ? @args["css"] : "issue-list"

      file_name      = "/wordify_cms/templates/issues_list"
      render_slim(context,
                  file_name,
                  {
                    :magazines => magazines.map(&:to_liquid),
                    :css       => css
                  })
    end
  end

end