# Adds Piwik tracking to a site.
#
# Use the tag like:
#
#  {% piwik_tracking %}
#
# In your Layout, ContentType or PageType.
#
class PiwikTracking < WordifyCms::Tag

  register_tag "piwik_tracking"

  def render(context)
    file_name = "/piwik/templates/tracking"
    render_template(context,file_name)
  end

end
