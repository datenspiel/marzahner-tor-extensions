# Piwik Extension

Adds Piwik tracking to the CMS by using the <code>piwik_analytics</code> gem.

### Install

Add

```
gem 'piwik_analytics', '~> 1.0.0'
```

to the Gemfile of your Rails app that uses Wordify.cms.

Run

```
bundle install
```

After that, generate the configuration file:

```
rails g piwik_analytics:install
```

### Configuration

Open up `config/piwik.yml` and edit the settings. Each setting is described in
the config file itself.

### Usage

In your

* content type
* page type
* layout

add

```
{% piwik_tracking %}
```
