# Public: Employee search extension configuration.
#
# Adds an extension to the CMS that crawls content by given urls with Casper.js.
# The crawler is a Node.js script that will be triggered by an observer if
# certain page objects are updated.
#
# The page name could be more generally but the above mentioned certain objects
# will content types which includes employee and contact people informations.
#
# The extension will enhance the Wordify.cms core search with its own data pool.
require 'yaml'
class EmployeeSearch < WordifyCms::Extension
  register_extension :type => :search

  # Searching EmployeeSearch Index is done by EmployeeSearchHandler class.
  search_handler EmployeeSearchHandler

  routes do
    match '/employee_search_configs' => 'employee_search_config#index',
          :via => :get
    match '/employee_search_configs/:id' => 'employee_search_config#update',
          :via => :put
  end

  # This is not a good solution, but far better than setting the host in any
  # observers.
  # Kinda bit hardcoded work here. :-|
  def self.search_host
    host_config_path = File.join(File.dirname(__FILE__), "config/hosts.yml")
    host_config = YAML.load_file(host_config_path)
    return host_config[Rails.env]
  end

end

WordifyCms::Account.class_eval do
  # Internal: 'master' account name validation to WordifyCms::Account.
  #
  # To have the JS extension afterHookIn working validate the account name
  # against the Marzahner-Tor super user account name to prevent similiar or
  # duplicated entries.
  validates_each :account_name do |record, attr, value|
    if record.account_name_changed?
      if value =~ /master/i
        record.errors.add(attr, I18n.t('wordify_cms.errors.models.account.not_available',
                                      {:name => value}))
      end
    end
  end

end

# Internal: Monkey patching WordifyCms::NavigationElement to enhance the class
# with two useful methods to get related navigation elements for a given path.
WordifyCms::NavigationElement.class_eval do

  # Public: Gets the root navigation element of an url slug.
  #
  # path - the slug or path the root navigation element is wanted for.
  #
  # Returns an instance of NavigationElement if a root is found otherwise nil.
  def self.root_for_path(path)
    get_attribute_for_path(path, :root)
  end


  # Public: Gets a page or page alias for a url slug.
  #
  # path - the slug or path the page or page alias is wanted for.
  #
  # Returns the page or page alias if any is found otherwise nil.
  def self.page_for_path(path)
    get_attribute_for_path(path, :page)
  end

  private


  # Internal: Gets an attribute or association of NavigationElement.
  #
  # path      - the slug or path the attribute or assciation is wanted for.
  # attribute - A Symbol representing the attribute or association name
  #
  # Returns the wanted attribute if found otherwise nil.
  def self.get_attribute_for_path(path, attribute)
    last_path = path.split("/").last
    node = self.where(:name => last_path.capitalize).first
    node.nil? ? nil : node.send(attribute)
  end

end