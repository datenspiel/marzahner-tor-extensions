
# Public: Crawling results persisted by the 'Tube' crawler. Mostly just for
# reading.
class EmployeeSearchIndex
  include ::Mongoid::Document
  include ::Mongoid::Timestamps

  store_in :collection => :employee_searches

  field :scope, :type => String
  field :data,  :type => String

end