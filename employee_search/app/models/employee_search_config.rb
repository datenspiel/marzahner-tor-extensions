# Public: Configuration model that contains the urls to be crawled.
class EmployeeSearchConfig
  include ::Mongoid::Document
  include ::Mongoid::Timestamps

  field :crawler_urls, :type => Array, :default => []

  class << self
    alias_method :configuration, :last
  end

  # Public: Updates the model with given crawler urls. The crawler urls are
  # cleaned up from whitespace before saving the model.
  #
  # url_ary - An Array of crawling urls
  #
  # Returns true if the model is saved otherwise false.
  def update_with_urls(url_ary)
    self.crawler_urls = url_ary.each{ |url| url.gsub!(/\s+/,"") }
    self.save
  end

end