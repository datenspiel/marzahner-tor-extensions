# Public: Observes instances of WordifyCms::DbModelInstance to start
# crawling configured pages that uses specific Model definitions of
# the WordifyCms::DbModelInstance instances.
class SearchDbModelInstanceObserver < Mongoid::Observer
  observe WordifyCms::DbModelInstance

  # Public: A little helper module containing allowedobservable model
  # definitions.
  module ObserveConfig
    extend self

    # Public: A list of allowed observable model definitions.
    #
    # Returns an Array with model definition names.
    def observable_models
      %w{ Department ContactPerson Scope Team FacilityManager}
    end

  end


  # Public: Inits a delayed crawling job if the model definition is
  # included in the allowed observable model definiton list. Otherwise it
  # won't do anything.
  def after_save(record)
    handle_crawling(record)
  end

  # Public: Inits a delayed crawling job after a model instance is removed from
  # the system.
  def after_destroy(record)
    handle_crawling(record)
  end

  private

  def handle_crawling(record)
    if record.class_definition.name.in?(ObserveConfig.observable_models)
      CatchTube.run
    end
  end
end