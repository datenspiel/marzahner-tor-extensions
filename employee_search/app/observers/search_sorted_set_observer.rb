# Public: Observes instances of WordifyCms::SortedSet to start crawling
# configured pages that uses instances of sorted set as content types.
class SearchSortedSetObserver < Mongoid::Observer
  observe WordifyCms::SortedSet

  # Public: Inits a delayed crawling job after a SortedSet is updated or
  # saved.
  def after_save(record)
    CatchTube.run
  end

  # Public: Inits a delayed crawling job after a SortedSet is removed from the
  # system.
  def after_destroy(record)
    CatchTube.run
  end


end