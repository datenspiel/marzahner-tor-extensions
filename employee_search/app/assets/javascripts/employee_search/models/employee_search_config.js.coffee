return unless Wordify?

# Public: Client side model of employee search configuration. Just contains
# the urls to crawl later.
class Wordify.EmployeeSearchConfig extends Wordify.Model
  @resourceName: 'employee_search_config'
  @persist Batman.RailsStorage

  @encode 'crawler_urls'

  # Public: An accessor to get the crawled urls formatted to use in textarea
  # elements, so there is only one url per line.
  #
  # Reassigns the entered urls back to #crawler_urls if the value changes.
  @accessor 'crawler_urls_formatted',
    get:(key)->
      return @get('crawler_urls').join("\n")
    set:(key,value)->
      @set('crawler_urls', value.split(/\n/))


