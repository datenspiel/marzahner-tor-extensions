#= require_tree ./controllers
#= require_tree ./models
return unless Wordify?

# Add an accessor to the user class.
Wordify.CurrentAccount.accessor 'isMaster?',
  get:(key)->
    (@get('account_name') is "master")

# Public: Extension configuration class for the client side backend extension
# that provides updating the crawling urls.
class Wordify.EmployeeSearch extends Wordify.Extension

  # Add backend route to handle search configuration
  @addRoute '/search/employees', 'search#index', as: 'search.employees'

  # Hook in at the end of the toolbar
  @hookIn {
    at          : 'toolbar',
    icon        : 'icon-white icon-search',
    locales     : 'wordify_cms.employee_search.name',
    identifier  : 'employee_search',
    route       : 'search.employees',
    afterHookIn : ()->
      Wordify.CurrentAccount.load (err, results) =>
        unless _.isEmpty(results)
          account = results[0];
          unless account.get('isMaster?')
            @removeHook('employee_search')
  }
  # Inside onlyIf 'this' is Wordify application
  # function will get a callback function that actually does the hooking