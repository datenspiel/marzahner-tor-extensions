return unless Wordify?
class Wordify.SearchController extends Wordify.Controller
  @beforeFilter only: ['index'], 'renderToolbar'
  @beforeFilter only: ['index'], 'renderUserNavigation'
  @beforeFilter only: ['index'], 'toggleBackendContent'

  @mixin Wordify.Controllerable

  routingKey: 'search'

  # Public: Serves the employee search configuration and renders the view.
  index:->
    Wordify.EmployeeSearchConfig.load (err,results) =>
      if err
        @checkBanned(err)
      else
        @set('employeeSearchConfig', results[0])
        @render source: 'search/index', into: 'main'

  # Public: Updates tne employee search configuration and renders a visual
  # feedback to to user.
  update:(params) ->
    @get('employeeSearchConfig').save (err) =>
      indexRoute  = Wordify.routeFor({routePath: 'search.employees'})
      errMsg      = I18n.t('wordify_cms.notifications.account.errors.update')
      notifyMsg   = I18n.t('wordify_cms.notifications.search.success')
      if err
        if err instanceof Batman.ErrorsSet
          alertify.err(errMsg)
        else
          @checkBanned(err)
      else
        options =
          location      : indexRoute
          notification  : notifyMsg
          translate     : no
        @redirectTo(options)
