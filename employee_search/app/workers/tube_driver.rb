# Public: Sidekiq worker to get crawling of pages starting.
class TubeDriver
  include Sidekiq::Worker
  sidekiq_options :backtrace => true,
                  :retry     => true

  # Public: Defines the action that will executed if #perform_async is called.
  #
  # Runs the node.js crawler script and logs the output to a Sidekiq log file.
  #
  # tube_path   - Node.js crawling script file path
  # page_config - file path location of crawlable pages
  # host        - The host where the crawling should happen
  #
  def perform(tube_path,page_config, host)
    logger.info {"I will perform my job."}
    cmd = %Q{ `which node` #{tube_path}/tube.js --page-config #{page_config} \
               --host #{host} --env #{Rails.env} }
    out = IO.popen(cmd.lstrip.rstrip)
    logger.info { out.readlines.join("\n")}
  end

end