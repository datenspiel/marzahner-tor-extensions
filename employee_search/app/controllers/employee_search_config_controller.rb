module WordifyCms

  # Public: Controller to serve and handle updating of the EmployeeSearchConfig
  # model.
  class EmployeeSearchConfigController < WordifyController

    respond_to :json

    # Public: Serves an instance of EmployeeSearchConfig. The model acts like
    # a singleton.
    def index
      @search = EmployeeSearchConfig.configuration
      respond_with(@search)
    end

    # Public: Updates the singleton instance of EmployeeSearchConfig.
    # Urls are cleaned from whitespace before saving.
    def update
      @search = EmployeeSearchConfig.find(params[:id])
      urls    = params["employee_search_config"]["crawler_urls"]
      @search.update_with_urls(urls)
      respond_with(@search)
    end
  end
end