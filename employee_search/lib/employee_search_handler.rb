
# Public: Handler class that try to find a search term in the crawling index.
class EmployeeSearchHandler


  # Public: Fetches all crawling results from the database and matches their
  # contents in a very simple regular expression way.
  #
  # term    - The search term.
  # context - A Liquid::Context instance coming from the
  #           SearchResultsTag class.
  #
  # Returns a Hash containing the search results.
  def run_search(term,context)
    employee_search_results = EmployeeSearchIndex.all
    result_data= []
    employee_search_results.each do |result|
      root_node = WordifyCms::NavigationElement.root_for_path(result.scope)
      page      = WordifyCms::NavigationElement.page_for_path(result.scope)
      encodeResult = MultiJson.load(result.data)
      fragment_data = encodeResult[result.scope]

      fragment_paths = collect_fragment_data(fragment_data,page,term)
      unless fragment_paths.empty?
        result_data << {
          :root   => root_node.name,
          :paths  => fragment_paths
        }
      end
    end
    {:path_results => result_data }
  end


  private


  # Internal: Iterates through collected search contents and compares the
  # content with the search term.
  #
  # data - collected search (html fragments) data
  # page - a page instance that references to the data
  # term - the search term
  #
  # Returns an Array of results.
  def collect_fragment_data(data,page,term)
    injector = lambda do |result,item|
      fragment = item.last["fragment"]
      if fragment.match(/#{term}/i)
        result << {
          :url => item.first,
          :name => page.title
        }
      end
      result
    end
    data.inject([],&injector)
  end

end