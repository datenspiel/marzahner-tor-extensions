# Public: Prepares crawling of destination pages by writing the pages config
# file and calling a delayed crawling job.
#
module CatchTube

  PAGE_CONFIG_PATH = "#{Dir.home}/tmp/employees_search/config"
  PAGE_CONFIG_NAME = "pages_config.json"

  # Public: "Runs" preparation of crawling, writes config and pass crawling
  # script required arguments to a delayed job worker.
  #
  # Returns nothing.
  def self.run
    write_config
    config_path = "#{PAGE_CONFIG_PATH}/#{PAGE_CONFIG_NAME}"
    tube_path = File.expand_path(File.join(File.dirname(__FILE__), "..", "node"))
    TubeDriver.perform_async(tube_path, config_path, EmployeeSearch.search_host)
  end

  private

  # Internal: Writes pages config to disk by loading the Search configuration.
  #
  # Returns nothing.
  def self.write_config
    unless File.exists?(PAGE_CONFIG_PATH)
      FileUtils.mkdir_p(PAGE_CONFIG_PATH)
    end
    config = EmployeeSearchConfig.configuration
    data = { :pages => [] }
    config.crawler_urls.each do |url_config|
      url,type = url_config.split(";")
      data[:pages] << {:url => url, :type => type}
    end
    json = MultiJson.dump(data)
    File.open("#{PAGE_CONFIG_PATH}/#{PAGE_CONFIG_NAME}", 'w') do |file|
      file.write(json)
    end
  end
end