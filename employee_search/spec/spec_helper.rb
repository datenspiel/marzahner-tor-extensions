require 'rubygems'
require 'rspec'

require 'mongoid'
require 'wordify.cms'

EXTENSION_ROOT = File.join(File.dirname(__FILE__),"..")

Dir[File.join(EXTENSION_ROOT,"app", "/**/*.rb")].each{ |f| require f }
Dir[File.join(EXTENSION_ROOT,"spec/support/**/*.rb", )].each{ |f| require f }

RSpec.configure do |config|

end