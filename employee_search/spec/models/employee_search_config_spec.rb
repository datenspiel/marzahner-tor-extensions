require 'spec_helper'

describe EmployeeSearchConfig do

  let(:employee_search_config){ EmployeeSearchConfig.new }

  it{ expect(employee_search_config).to respond_to(:crawler_urls) }
  it{ expect(EmployeeSearchConfig).to respond_to(:configuration)}
  it{ expect(EmployeeSearchConfig).to respond_to(:search_handler)}

  <<-DOC
  describe "#configuration" do
    it{ expect(EmployeeSearchConfig).to be_empty }
  end
  DOC

  describe "its default values for attributes" do

    context "#crawler_urls" do
      it{ expect(employee_search_config.crawler_urls).to be_empty }
    end

  end

end