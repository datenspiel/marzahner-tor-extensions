# Employee Search Extension

This extension provides a custom search based on its own crawling results.

## Prerequirements

You have to install [Redis](http://redis.io) to have this extension working.
See the **Notes ....** section for further informations.

## How it works

The integrated page crawler takes a bunch of urls that can be specified in
two ways:

* dynamic (have some AJAX)
* static (just grab HTML fragments)

Crawling destinations and their types can be configured within the CMS backend
(navigation entry **Mitarbeiter Suche**).

After any content is changed at the specific sites the crawling is fired.

[Sidekiq](https://github.com/mperham/sidekiq) is used for starting the crawling,
so make sure that you have [Redis](http://redis.io) installed and running.


## Running the crawler

The crawler is a Node.js script called **Tube** that uses PhantomJS to have
access the resulting HTML Snapshot of the above mentioned pages.

```
`which node` tube.js  --page-config $HOME/config/page_config.json \
                      --host http://www.marzahner-tor.de \
                      --env production
```

## Notes about the skeleton application that uses the extension

To have the skeleton application that includes this extension working without
headaches try the following.

Add

```ruby
gem "foreman"
```

to your Gemfile.

Run <code>bundle install</code>

Add a new file called <code>Procfile</code> to the root of your skeleton and add:

```
web: unicorn_rails -c config/unicorn/development.rb
sidekiq: bundle exec sidekiq - C config/sidekiq.yml
cache: $REDIS_BIN_PATH $REDIS_CONF_PATH
```

You may be at the end of the path, just a few configurations left.

Add a Unicorn configuration file <code>development.rb</code> to <code>Rails.root/config/unicorn</code>:

```ruby
worker_processes 2
```

If you want more processes, feel free to adjust them.

In <code>Rails.root/config/sidekiq.yml</code> add:

```yml
---
:logfile: ./log/sidekiq.log
```

At least, add a hidden <code>Foreman</code> configuration file <code>.env</code>
to the root of your skeleton application:

```
REDIS_BIN_PATH="/usr/local/bin/redis-server"
REDIS_CONF_PATH='/usr/local/etc/redis.conf'
```

Adjust the paths to your needs.

After all, run

```
foreman start
```

to have the application running with all required dependencies.
