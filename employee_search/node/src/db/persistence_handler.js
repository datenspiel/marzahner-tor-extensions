require('./connection');

var fs                  = require('fs'),
    mongoose            = require("mongoose"),
    path                = require("path"),
    EmployeeSearchModel = require('../models/employee_search'),
    Announcer           = require('../util/announcer').Announcer;

/*
* Public: Handler that persists data to a  MongoDb.
*/
function PersistenceHandler(){}

/*
* Public: Stores extracted data from a destination to the database.
*
* data        - Data object to be stored. Before saving it will be converted to
*               a JSON String.
* destination - The crawled destination that will be used as scope for data.
* cb          - callback function used after updating or saving data.
*
*/
PersistenceHandler.prototype.store = function(data, destination, cb){
  var me = this,
      stringifiedData = JSON.stringify(data);

  EmployeeSearchModel.findOne({"scope": destination},'scope data', function(err, obj){
    if(err){ return handleError(err) }
    if(obj){

      obj.data = stringifiedData;
      obj.save(cb);
    }else{
      Announcer.announceLostAndFindFor(destination);
      var model   = new EmployeeSearchModel();
      model.data  = stringifiedData;
      model.scope = destination;
      model.save(cb);
    }
  });
}

module.exports.PersistenceHandler = PersistenceHandler