var mongoose = require('mongoose'),
    path     = require('path'),
    fs       = require('fs');

/*
* Public: Class that handles database connection configuration.
*
* Configuration file is located at config/mongo.json.
* Environment handling is done by global var CRAWL_ENV
*/
function DbConfigHandler(){
  this.configPath = path.join(__dirname, '../../', 'config/mongo.json')
}

/*
* Public: Reads configuration file in ablocking way.
*
* Returns the MongoDB uri string;
*/
DbConfigHandler.prototype.configuration = function(){
  var config = JSON.parse(fs.readFileSync(this.configPath, "utf8"));
  return this._buildMongoURIFrom(config);
}

/*
* Internal: Builds mongodb uri from configuration object.
*
* config  - Object containing db configuration values.
*
* Note: user/Password isn't supported yet.
*
* Returns the MongoDB uri string;
*/
DbConfigHandler.prototype._buildMongoURIFrom = function(config){
  var configData = config[CRAWL_ENV],
      uri        = "mongodb://";

  uri += configData["host"];
  uri += ":" + configData["port"];
  uri += "/";
  uri += configData["database"];

  return uri;
}

var dbConfig = new DbConfigHandler();

module.exports = mongoose.connect(dbConfig.configuration());