var PersistenceHandler = require('../db/persistence_handler').PersistenceHandler;

/*
* Public: Base parser. Provides ivars and datastructure that is stored to
* the db backend..
*/
function Parser(){}

/*
* Public: Inits the parser
*
* crawler       - The Crawler instance that provides a PhantomJS instance.
* page          - Webpage instance
* destination   - The Url the webpage should represent.
*
*/
Parser.prototype.init = function(crawler,page,destination){
  this.crawler      = crawler;
  this.page         = page;
  // destination is the tree master key for persisting data!
  this.destination  = destination;
  this.resultSize   = 0;
  this.currentIndex = 0;

  this.persistenceHandler = new PersistenceHandler();
  this._initializeDataObject();
  return this;
}

/*
* Internal: Initialises the data structure.
*
* Examples
*
*   {
*     "/my/page_destination" : {}
*   }
*/
Parser.prototype._initializeDataObject = function(){
  this.data = {}
  this.data[this.destination] = {}
}

module.exports.Parser = Parser;