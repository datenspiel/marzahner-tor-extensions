var Parser              = require('./parser.js').Parser,
    JavaScriptInjector  = require('../util/injector').JavaScriptInjector,
    Doorkeeper          = require('../util/doorkeeper').Doorkeeper,
    Announcer           = require('../util/announcer.js').Announcer;

/*
* Public: Parser for parsing site with AJAX content.
*/
function DynamicParser(){ }

DynamicParser.prototype = new Parser();

/*
* Public: Parses the page.
*
* Since the page that sould be parsed contains some AJAX requests, the parse
* function will save all option values of a HTML select tag into an array and
* will recall the page (by #callPage()) with very single value attached:
*
*   /my/page#department_id=1234567
*   /my/page#department_id=2345663
*
* url  - the page's url that should be parsed
* Returns the duplicated String.
*/
DynamicParser.prototype.parse = function(url){
  var me,
      evaluate,
      persistData;

  me        = this;

  evaluate  = function(){
    var j = jQuery.noConflict();
    var streetIds = [];
    $("#adress-select option").each(function(index){
      if(index != 0){
        streetIds.push($(this).val());
      }
    });
    return streetIds;
  }

  persistData = function(results){
    me.resultSize   = results.length;
    results.forEach(function(item,index){
      me.callPage(item,index)
    });
  }

  me.page.evaluate(evaluate, persistData);
}

/*
* Public: Recalls the destination page url with a hash fragment attached.
*
* It stores the extracted html fragments in a datastructure and passes that
* structure to a persistence handler after all fragments are extracted from
* the page for every single value.
*
*/
DynamicParser.prototype.callPage = function(id,index){
  var me            = this,
      concatedUrl   = this.crawler.host + this.destination +
                      "#department_id=" + id;;

  me.crawler.ph.createPage(function(page){
    page.open(concatedUrl, function(status){
      JavaScriptInjector.injectjQueryFor(page);
      var evaluatePage = function(){
        return $('.departments-list').html();
      }
      page.evaluate(evaluatePage, function(result){
        if(result===null){
          Announcer.announceBreakdown(me.destination);
          return;
        }
        me.data[me.destination][concatedUrl] = {
          "fragment" :result
        };
        if(index == (me.resultSize-1)){
          me.persistenceHandler.store(me.data,me.destination, function(err){
            if(err){
              throw err;
            }else{
              Announcer.announceRecordSaved();
              Announcer.announceDestinationReachedFor(me.destination);
              Doorkeeper.unlockDoorFor(me.destination);
            }
          });

        }
      });
    })
  });
}

module.exports.DynamicParser = DynamicParser;