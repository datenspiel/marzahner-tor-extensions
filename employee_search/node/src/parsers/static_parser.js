var Parser              = require('./parser.js').Parser,
    JavaScriptInjector  = require('../util/injector').JavaScriptInjector,
    Doorkeeper          = require('../util/doorkeeper').Doorkeeper,
    Announcer           = require('../util/announcer.js').Announcer;
/*
* Public: Parser that just extract a HTML fragment from a page.
*
*/
function StaticParser(){ }

StaticParser.prototype = new Parser();

/*
* Public: Starts evaluating and parsing the webpage by a given url. After
* evaluating the pages document and extracting the fracment it's data
* structure is passed to a persistence handler and stored to the database
* backend.
*
* url  - The Url the page should be evaluating.
*
*/
StaticParser.prototype.parse = function(url){
  var me        = this,
      evaluate;

  JavaScriptInjector.injectjQueryFor(me.page);

  evaluate    = function(){
    return $('.main-content').html()
  }

  me.page.evaluate(evaluate, function(result){
    if(result===null){
      Announcer.announceBreakdown(me.destination);
      return;
    }
    me.data[me.destination][me.destination] ={
      fragment : result
    };


    me.persistenceHandler.store(me.data, me.destination, function(err,obj){
      if(err){
        throw(err);
      }else{
        console.log("Record saved.");
        Announcer.announceDestinationReachedFor(me.destination);
        Doorkeeper.unlockDoorFor(me.destination);
      }
    });
  });
}

module.exports.StaticParser = StaticParser;