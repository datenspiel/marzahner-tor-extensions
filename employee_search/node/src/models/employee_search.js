var mongoose = require('mongoose');

/*

*/
var employeeSearchSchema = new mongoose.Schema({
  data        : String,
  scope       :   String,
  updated_at  :   {
    type    : Date,
    default : Date.now
  }
},{
  collection : 'employee_searches'
});

module.exports = mongoose.model("EmployeeSearch", employeeSearchSchema);