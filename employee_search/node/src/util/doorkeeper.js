var _         = require('underscore'),
    Announcer = require('./announcer.js').Announcer;

/*
* Public: Some kind of guard that holds references to running crawl processes.
* The guard releases references and if its bucket is empty, forces the exit
* the global process.
*/
var Doorkeeper = function(){}

// Bucket for holding crawl processes identified by the crawlers destination.
Doorkeeper.lockedDoors    = [];
// Action what the doorkeeper shhould do if all processes are released.
Doorkeeper.actsOnLastStop = undefined;

/*
* Public: Locks the tube's door for its destination. Means, adds a destination
* identifier to the locked process bucket.
*
* Note that distinguishing of unique destinations is not supported.

* destination - page's url which is crawled at the moment..
*
*/
Doorkeeper.lockDoorFor = function(destination){
  Announcer.announceClosedDoors(destination);
  var doorAlreadyLocked = _.contains(this.lockedDoors, destination);
  if(!doorAlreadyLocked){
    this.lockedDoors.push(destination);
  }
}

/*
* Public: Unlocks the tube's door for its destination. Means, removes a
* destination identifier from the locked process bucket. This is typical used
* to mark a crawl process as finished.
*
* If all processes are released the doorkeeper calls the action that is
* assigned to #actsOnLastStop.
*
* destination - page's url which was crawled
*/
Doorkeeper.unlockDoorFor = function(destination){
  Announcer.announceOpenedDoors(destination)
  var doorLockedIndex = _.indexOf(this.lockedDoors, destination);
  if(doorLockedIndex != -1){
    this.lockedDoors.splice(doorLockedIndex, 1);
  }
  if(this.hasUnlockedAllDoors()){
    if(typeof this.actsOnLastStop === "function"){
      this.actsOnLastStop();
    }
  }
}

Doorkeeper.hasUnlockedAllDoors = function(){
  return (this.lockedDoors.length == 0);
}

module.exports.Doorkeeper = Doorkeeper;