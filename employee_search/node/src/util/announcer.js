var winston       = require('winston'),
    winstonOtions = {
      filename  : global.LOGGER_ENV.file,
      dirname   : global.LOGGER_ENV.dir
    };
winston.add(winston.transports.File, winstonOtions);
winston.remove(winston.transports.Console);

function Announcer(){}

Announcer.useTwitter = false;

Announcer.announceStartFor              = function(destination){
  winston.info("Start crawling towards " + destination + ".")
}

Announcer.announceDestinationReachedFor = function(destination){
  winston.info("Crawling ends for " + destination + ".");
}

Announcer.announceWarmWelcome           = function(){
  winston.info("--- Let's have a ride ...")
}

Announcer.annouceFarewell               = function(){
  winston.info("--- Destinations reached. Good bye and have a nice day.");
}

Announcer.announceClosedDoors           = function(destination){
  winston.info("Line towards " + destination + ", doors are locked now.");
}

Announcer.announceOpenedDoors           = function(destination){
  winston.info("Line towards " + destination + ", doors are unlocked now.");
}

Announcer.announceRecordSaved           = function(){
  winston.info("Record saved.");
}

Announcer.announceLostAndFindFor        = function(destination){
  winston.info("Found existing data for " + destination + ".");
}

Announcer.announceBreakdown             = function(destination){
  if(this.useTwitter){
    this.announceToTwitter("breakdown");
  }
  winston.info("Some error occurred during crawling towards " + destination);
}

Announcer.announceToTwitter             = function(status){
  var Twitter   = require('twit'),
      T         = new Twitter(global.TWITTER_CONF["twitter_config"]),
      tweet     = global.TWITTER_CONF["updates"][status];

  tweet     = tweet.replace("{%user%}", global.TWITTER_CONF["to"]);
  tweet     = tweet.replace("{%date%}", new Date().toTimeString());

  T.post('statuses/update', { status: tweet }, function(err, reply) {
    if(err){
      winston.info("Some error occured during tweeting: " + err);
    }
    if(status === "breakdown"){
      process.exit();
    }else{
      return;
    }
  });
}

module.exports.Announcer = Announcer;
