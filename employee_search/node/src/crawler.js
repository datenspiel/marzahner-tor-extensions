var phantom             = require('phantom');
    PersistenceHandler  = require('./db/persistence_handler.js')
                          .PersistenceHandler,
    DynamicParser       = require('./parsers/dynamic_parser.js')
                          .DynamicParser,
    StaticParser        = require('./parsers/static_parser.js')
                          .StaticParser,
    JavaScriptInjector  = require('./util/injector')
                          .JavaScriptInjector,
    _                   = require('underscore'),
    Doorkeeper          = require('./util/doorkeeper').Doorkeeper,
    Announcer           = require('./util/announcer.js').Announcer;

/*
 * Public: Class that opens a page url in a headless browser and fires up
 * a parser based on the destinations parsing type (could be static, dynamic).
 *
 * * options - A JavaScript object for defining the Crawlers ivars.
 *              * phantom     - PhantomJs instance (headless browser)
 *              * destination - URL to be parsed
 *              * host        - domain that prefixes destination
 *              * parseWith   - Parsertype (static,dynamic)
*/
Crawler = function(options){
  this.phantom      = options.phantom;
  this.destination  = options.destination;
  this.host         = options.host;
  this.ph           = this.phantom;
  this.parseWith    = options.parseWith;

  this.parsers      = {
      "static"  : new StaticParser(),
      "dynamic" : new DynamicParser(),
  }

  this.locked = true;
}
/*
* Internal: Crawls the page.
*
* destination   - URL to be parsed
* page          - instance of PhantomJS webpage
*
*/
Crawler.prototype._crawl = function(destination, page){
  var me  = this;
  var url = this.host + destination;
  page.open(url, function(status){
    JavaScriptInjector.injectjQueryFor(page)
    //THIS dependes on which site type located in the config
    var parser = me.parsers[me.parseWith].init(me,page,destination);
    parser.parse(url);
    //me.extractAndPersistData(page);
  });
}

/*
* Public: Starts crawling a website.
*
*/
Crawler.prototype.startCrawling = function(crawlerQeuePos){
  this.crawlerQeuePos = crawlerQeuePos;
  var me = this;
  var pageHandling = function(page){
      Announcer.announceStartFor(me.destination);
      me._crawl(me.destination,page);
    };
  this.phantom.createPage(pageHandling);

}

module.exports.Crawler = Crawler