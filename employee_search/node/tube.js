var argv            = require('optimist'),
    fs              = require('fs'),
    phantomInstance = require('phantom'),
    path            = require('path'),
    portscanner     = require('portscanner'),
    cli             = argv.demand('page-config')
                          .describe('page-config', 'Pages to crawl!')
                          .demand('host')
                          .describe('host', 'Server host to reach for crawling')
                          .default('env', 'development')
                          .argv
                          ;

/*
* Public: Main function to start crawling of given destinations (urls).
*
* The destinations are given by a page config file. It's file path is given by
* the commandline argument page-config.
*
* All arguments:
*
* * host        - The domain name to access the destinations (required)
* * page-config - The destinations config file. This file also includes the
*                 parsing type. See below for an example file. (required)
* * env         - The server and database environment. Defaults to development.
*
* Running the script:
*
*   node tube.js --page-config SHOME/configs/pages.json \
*                --host http://localhost:8080 \
*                --env production
*
* Destination config file have to be a json file.
*
*   {
*     "pages" : [
*       {
*         "url"       : "/mieter/ansprechpartner",
*         "type"      : "dynamic"
*       },
*       {
*         "url"   : "/wohnfuehlgenossenschaft/wir-ueber-uns/mitarbeiter",
*         "type"  : "static"
*       }
*     ]
*   }
*
* Database configuration is possible through config/mongo.json
*/
(function(){
  var host                  = cli['host'],
      pageConfigLocation    = cli['page-config'],
      twitterConfigLocation = __dirname + "/config/twitter.json";

  global.CRAWL_ENV          = cli['env'];

  fs.readFile(__dirname + "/config/logger.json", "utf8", function(err,data){
    if(err){
      console.log('Error parsing logger config file : ' + err);
      process.exit();
    }

    var loggerConfig    = JSON.parse(data);

    global.LOGGER_ENV   = loggerConfig[global.CRAWL_ENV];
    global.TWITTER_CONF = {}

    if(!global.LOGGER_ENV.hasOwnProperty("dir")){
      global.LOGGER_ENV.dir = __dirname + "/log";
    }

    var Announcer             = require('./src/util/announcer.js').Announcer,
        Doorkeeper            = require('./src/util/doorkeeper').Doorkeeper,
        Crawler               = require('./src/crawler').Crawler;

        Announcer.useTwitter  = true;

    /***************************************************************************
    * Read twitter config and start processing pages config                    *
     **************************************************************************/
    fs.readFile(twitterConfigLocation,"utf8", function(err,data){
      if(Announcer.useTwitter){
        if(err){
          console.log("Error parsing twitter config file: " + err);
          process.exit();
        }
        global.TWITTER_CONF = JSON.parse(data);

      }

      /***********************************************************************
       * Read pages config and start crawling                                *
       **********************************************************************/
      fs.readFile(pageConfigLocation, "utf8", function(err,data){
        if (err) {
          console.log('Error parsing page config file : ' + err);
          process.exit();
        }

        var config          = JSON.parse(data),
            configItemSize  = config["pages"].length;

        global.PAGES_TO_CRAWL = configItemSize;

        Doorkeeper.actsOnLastStop = function(){
          Announcer.annouceFarewell();
          process.exit();
        }

        var phantomAccessr = function(phantom){
          config["pages"].forEach(function(configItem, index){
            var crawler = new Crawler({
                                phantom     : phantom,
                                destination : configItem.url,
                                host        : host,
                                parseWith   : configItem.type
                              });
            Doorkeeper.lockDoorFor(configItem.url);
            crawler.startCrawling(index);
          })
        };
        var minPort = 40000,
            maxPort = 60000,
            phHost  = 'localhost';
        portscanner.findAPortNotInUse(minPort,maxPort,phHost, function(err, port){
          phantomInstance.create(phantomAccessr, {
            port            : port,
            cookiesEnabled  : false
          });
        });
      });
    });
  });
})();



